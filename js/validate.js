function validate(event) {
  // TODO - write custom validation logic to validate the longitude and latitude
  // values. The latitude value must be a number between -90 and 90; the
  // longitude value must be a number between -180 and 180. If either/both are
  // invalid, show the appropriate error message in the form, and stop the 
  // form from being submitted. If both values are valid, allow the form to be
  // submitted.
  let latVal = document.querySelector('#add-obs').lat.value;
  let lngVal = document.querySelector('#add-obs').lng.value;
  let latAlert = document.querySelector('.lat');
  let lngAlert = document.querySelector('.lng');
  let flag1 = false, flag2 = false;
  console.log('TODO - validate the longitude, latitude values before submitting');
  if(latVal >= -90 && latVal <= 90){
    flag1 = true;
    latAlert.hidden = true;
  } else {
    flag1 = false;
    latAlert.hidden = false;
  }

  if(lngVal >= -180 && lngVal <= 180){
    flag2 = true;
    lngAlert.hidden = true;
  } else {
    flag2 = false;
    lngAlert.hidden = false;
  }

  if(flag1 === true && flag2 === true){
    return true;
  } else {
    event.preventDefault();
    return false;
  }

}

// Wait for the window to load, then set up the submit event handler for the form.
window.onload = function() {
  const form = document.querySelector('form');
  form.onsubmit = validate;
};
